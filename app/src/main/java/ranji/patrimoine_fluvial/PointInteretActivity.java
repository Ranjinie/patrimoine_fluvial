package ranji.patrimoine_fluvial;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PointInteretActivity extends AppCompatActivity {
    String commune;
    ArrayList<PointInteret> lesPointInteret = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_interet);

        commune = this.getIntent().getExtras().getString("commune");

        lesPointInteret = listePointInteret(this);


    }


    private ArrayList<PointInteret> listePointInteret(Context context) {
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open("JSON_Allege.json")));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String myjsonstring = sb.toString();
        try {
            JSONArray jsonArray = new JSONArray(myjsonstring);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONObject fields = (JSONObject) jsonObject.get("fields");
                String id = i + "";
                String commune = fields.getString("commune");
                String ensem2 = fields.getString("ensem2");
                String histo1 = fields.getString("histo1");
                String elem_princ = fields.getString("elem_princ");
                String elem_patri = fields.getString("elem_patri");

                PointInteret pointInteret = new PointInteret(id, commune, ensem2, histo1, elem_princ, elem_patri);
                lesPointInteret.add(pointInteret);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return lesPointInteret;
    }


}
