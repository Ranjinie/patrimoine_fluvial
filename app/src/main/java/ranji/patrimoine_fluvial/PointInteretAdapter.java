package ranji.patrimoine_fluvial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PointInteretAdapter extends ArrayAdapter<String> {
    private ArrayList<String> pointinterets;
    private Context context;

    public PointInteretAdapter(Context context, ArrayList<String> pointinterets) {
        super(context, R.layout.item_pointinteret, R.id.pointinteret, pointinterets);
        this.pointinterets = pointinterets;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_pointinteret, parent, false);
        }
        TextView texte = convertView.findViewById(R.id.pointinteret);

        texte.setText(getItem(position));

        //Handle buttons and add onClickListeners
        Button favBtn = (Button)convertView.findViewById(R.id.favori_btn);

        final String leFavori = pointinterets.get(position);

        favBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                // Setting Dialog Title
                alertDialog.setTitle(leFavori);
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_star_black_24dp);

                // Setting Dialog Message
                alertDialog.setMessage("Ajouter à vos favoris ?");
                alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("favoris", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(leFavori, "marse");
                        editor.apply();
                        Toast.makeText(context, "Ce point d'intérêt a été ajouté à vos favoris", Toast.LENGTH_SHORT).show();
                        Intent retour = new Intent(context,context.getClass());
                        context.startActivity(retour);
                        ((Activity) context).finish();


                    }
                });

                alertDialog.setNeutralButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                // Showing Alert Message
                alertDialog.show();
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
