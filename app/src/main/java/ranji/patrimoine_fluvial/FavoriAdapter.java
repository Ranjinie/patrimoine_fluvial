package ranji.patrimoine_fluvial;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class FavoriAdapter extends ArrayAdapter<String> {
    private ArrayList<String> favoris;
    private Context context;

    public FavoriAdapter(Context context, ArrayList<String> favoris) {
        super(context, R.layout.item_favori, R.id.favori, favoris);
        this.favoris = favoris;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_favori, parent, false);
        }
        TextView texte = convertView.findViewById(R.id.favori);

        texte.setText(getItem(position));

        //Handle buttons and add onClickListeners
        Button deleteBtn = (Button)convertView.findViewById(R.id.delete_btn);

        final String leFavori = favoris.get(position);

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                // Setting Dialog Title
                alertDialog.setTitle(leFavori);
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_delete_forever_black_24dp);

                // Setting Dialog Message
                alertDialog.setMessage("Supprimer ce favori ?");
                alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("favoris", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.remove(leFavori);
                        editor.apply();
                        Toast.makeText(context, "Favori supprimé ", Toast.LENGTH_SHORT).show();
                        Intent retour = new Intent(context,context.getClass());
                        context.startActivity(retour);
                        ((Activity) context).finish();


                    }
                });

                alertDialog.setNeutralButton("Non", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                // Showing Alert Message
                alertDialog.show();
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
