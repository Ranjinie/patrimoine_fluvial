package ranji.patrimoine_fluvial;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_communes) {
            fragment = new CommunesFragment();
        }
        else if (id == R.id.nav_favoris) {
            SharedPreferences sharedPreferences = getSharedPreferences("favoris", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
//            if(sharedPreferences.contains("Paris")){
//                Toast.makeText(this, sharedPreferences.getString("Paris", null), Toast.LENGTH_SHORT).show();
//                editor.clear();
//                editor.apply();
//            }else{
//                HashSet<String> favs = new HashSet<String>();
//                favs.add("e");
//                Toast.makeText(this, "Nouvelle preference", Toast.LENGTH_SHORT).show();
//                editor.putString("Paris", "Pont ");
//                editor.apply();
//            }
            Log.i("size", Integer.toString(sharedPreferences.getAll().size()));
            if (sharedPreferences.getAll().size() == 0) {
                Toast.makeText(this, "Aucun point d'intérêt favori", Toast.LENGTH_SHORT).show();
                fragment = new FavorisFragment();
            }else{
                // Test pour retirer un élément
                // editor.remove("e");
                // editor.apply();
                fragment = new FavorisFragment();
            }
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
