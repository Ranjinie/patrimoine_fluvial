package ranji.patrimoine_fluvial;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PointsInteretFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PointsInteretFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PointsInteretFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private String ville;
    private ListView lv;
    private ArrayList<String> listPontInteret;
    public PointsInteretFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PointsInteretFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PointsInteretFragment newInstance(String param1, String param2) {
        PointsInteretFragment fragment = new PointsInteretFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_points_interet, container, false);
        Log.i("ville_test_arg", getArguments().getString("ville"));
        ville = getArguments().getString("ville");

        listPontInteret = listPointInteret(getContext());

        ArrayAdapter arrayAdapter = new PointInteretAdapter(getActivity(), listPontInteret);

        lv = v.findViewById(R.id.list_point_interet);

        lv.setAdapter(arrayAdapter);

        Log.i("arraylist",listPontInteret.toString());

        // Inflate the layout for this fragment
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private ArrayList<String> listPointInteret(Context context){
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        ArrayList<String> returnedArray = new ArrayList<String>();
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open("JSON_Allege.json")));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        }catch(IOException e){
            e.printStackTrace();
        } finally {
            try {
                br.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        String myjsonstring = sb.toString();

        try{
            JSONArray jsonRootObject = new JSONArray(myjsonstring);

            for(int i=0; i <= jsonRootObject.length() - 1; i++) {

                JSONObject jsonObject = new JSONObject(jsonRootObject.get(i).toString());

                JSONObject child_obj = new JSONObject(jsonObject.getString("fields"));


                if(child_obj.getString("commune").equals(this.ville)){
                    returnedArray.add(child_obj.getString("elem_patri"));
                    Log.i("commune",child_obj.getString("elem_patri"));

                }



            }

//            Iterator<?> keys = jsonRootObject.keys();
//            JSONArray jsonArray = jsonRootObject.getJSONArray("");
//            while( keys.hasNext() ) {
//
//                JSONArray jsonArray1 = jsonRootObject.getJSONArray((String) keys.next());
//
//                for(int i=0; i <= jsonArray1.length(); i++) {
//                    Log.i("TESTTESTTEST","erer");
//                }
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnedArray;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
