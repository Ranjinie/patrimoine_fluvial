package ranji.patrimoine_fluvial;

public class PointInteret {

    public final String id;
    public final String commune;
    public final String ensem2;
    public final String histo1;
    public final String elem_princ;
    public final String elem_patri;

    public PointInteret(String id, String commune, String ensem2, String histo1, String elem_princ, String elem_patri) {
        this.id = id;
        this.commune = commune;
        this.ensem2 = ensem2;
        this.histo1 = histo1;
        this.elem_princ = elem_princ;
        this.elem_patri = elem_patri;
    }


    public String getId() {
        return id;
    }

    public String getCommune() {
        return commune;
    }

    public String getEnsem2() {
        return ensem2;
    }

    public String getHisto1() {
        return histo1;
    }

    public String getElem_princ() {
        return elem_princ;
    }

    public String getElem_patri() {
        return elem_patri;
    }

    @Override
    public String toString() {
        return elem_patri;
    }




}